<div class="uk-main-wrap mt-2">
    <div class="container">
        <div class="bg-white uk-score-rule p-3">
            <h1 class="text-center mb-3 font-15">{$setting.site_name}积分规则</h1>
            {volist name="list" id="v"}
            <dl class="overflow-auto">
                <dt class="float-left">{$v.title}</dt>
                <dd class="float-right">
                    <span class="{$v.score >0 ? 'text-success' : 'text-danger'}">{$v.score} 积分</span>

                    <span class="ml-4 text-danger">
                        {if $v['cycle_type']=='day' && $v['cycle']}
                        每<span class="font-weight-bold">{$v['cycle']}</span>天只可获得一次
                        {/if}

                        {if $v['cycle_type']=='week' && $v['cycle']}
                        每<span class="font-weight-bold">{$v['cycle']}</span>周只可获得一次
                        {/if}

                        {if $v['cycle_type']=='month月' && $v['cycle']}
                        每<span class="font-weight-bold">{$v['cycle']}</span>月只可获得一次
                        {/if}

                        {if $v['cycle_type']=='hour' && $v['cycle']}
                        每<span class="font-weight-bold">{$v['cycle']}</span>小时只可获得一次
                        {/if}

                        {if $v['cycle_type']=='minute' && $v['cycle']}
                        每<span class="font-weight-bold">{$v['cycle']}</span>分钟只可可获得一次
                        {/if}

                        {if $v['cycle_type']=='second' && $v['cycle']}
                        每<span class="font-weight-bold">{$v['cycle']}</span>秒只可可获得一次
                        {/if}

                        {if !$v['cycle']}
                        <span class="font-weight-bold">不限次数</span>
                        {/if}
                    </span>
                </dd>
            </dl>
            {/volist}
        </div>
    </div>
</div>