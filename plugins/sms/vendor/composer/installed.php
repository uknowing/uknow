<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '824834a6540f367db8293c845aefc8988dad674b',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '824834a6540f367db8293c845aefc8988dad674b',
    ),
    'adbario/php-dot-notation' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eee4fc81296531e6aafba4c2bbccfc5adab1676e',
    ),
    'alibabacloud/credentials' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c30db1ddf07318b9ee79aa7af46e0bd6be697da',
    ),
    'alibabacloud/darabonba-openapi' => 
    array (
      'pretty_version' => '0.1.8',
      'version' => '0.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c577dea4415b6812d52d9e970a517932eed4a997',
    ),
    'alibabacloud/dysmsapi-20170525' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99a0098c844af962147a765abd4dc967ff311a1c',
    ),
    'alibabacloud/endpoint-util' => 
    array (
      'pretty_version' => '0.1.1',
      'version' => '0.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3fe88a25d8df4faa3b0ae14ff202a9cc094e6c5',
    ),
    'alibabacloud/openapi-util' => 
    array (
      'pretty_version' => '0.1.7',
      'version' => '0.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8b795b61049ad3aac27434b19d637e128d596c4',
    ),
    'alibabacloud/tea' => 
    array (
      'pretty_version' => '3.1.21',
      'version' => '3.1.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '379faffe240ee97134cf3f796cb28059f9fb7fa9',
    ),
    'alibabacloud/tea-utils' => 
    array (
      'pretty_version' => '0.2.14',
      'version' => '0.2.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '381df15cb4bdb58dbf596f94869ffd2ef680eddd',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.3.0',
      'version' => '7.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7008573787b430c1c1f650e3722d9bba59967628',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '35ea11d335fd638b5882ff1725228b3d35496ab1',
    ),
    'lizhichao/one-sm' => 
    array (
      'pretty_version' => '1.9',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2e4c57af85ffa763cd0896cc58b5596d2d283c46',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'tencentcloud/common' => 
    array (
      'pretty_version' => '3.0.367',
      'version' => '3.0.367.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4684787e6acc055e223fdc820f9f6f5c8a05ca4',
    ),
    'tencentcloud/sms' => 
    array (
      'pretty_version' => '3.0.367',
      'version' => '3.0.367.0',
      'aliases' => 
      array (
      ),
      'reference' => '42432d961a38dc0e3855f3edfcfbe402b58c5101',
    ),
  ),
);
