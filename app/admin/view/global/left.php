    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-default elevation-4">
        <!-- Brand Logo -->
        <a href="{:url('Index/index')}" class="brand-link">
            {if get_setting('site_logo') && !get_setting('logo_type')}
            <img src="{:get_setting('site_logo','/static/common/image/logo.png')}" class="brand-image">
            {else/}
            <span class="font-weight-bolder">{:get_setting('site_name')}</span>
            {/if}
        </a>
        <div class="sidebar" <!--style="margin-top: 0"-->>
            <nav class="mt-2 mb-2">
                <ul class="sidebar-menu nav nav-pills no_radius nav-sidebar flex-column nav-child-indent js_left_menu_show" data-widget="treeview" role="menu" data-accordion="true">
                    {$_menu|raw}
                </ul>
            </nav>
        </div>
    </aside>